# OpenAirInterface CN AUSF - Quantum Safe Fork

This repository is a fork of the master branch from the official OpenAirInterface CN AUSF repository. It aims to enhance security by integrating encryption and post-quantum algorithms into AUSF network functions, preparing for the era of quantum computing.

## Prerequisites

Docker is required for building and running the AUSF images. Please ensure Docker is installed on your system before proceeding.

## Installation

1. Clone the repository:
   ```
   git clone https://gitlab.eurecom.fr/maxime.six/oai-cn-5-g-ausf-quantum-safe
   ```
2. Navigate into the cloned directory:
   ```
   cd oai-cn-5-g-ausf-quantum-safe
   ```
3. Build the Docker image:
   ```
   docker build -t oai-ausf-image -f docker/Dockerfile.ausf.ubuntu .
   ```

## Running

To deploy the AUSF service using Docker, execute the following command:
```
docker run -d --name ausf-oqs oai-ausf-image
```

## Repository Structure

The repository is organized as follows:

```
openair-cn5g-ausf
├── 3gpp-specs:     Directory containing 3GPP specification files (YAML) for AUSF implementation.
├── build:          Build directory with targets and object files from network function compilation.
    ├── log:        Log files from builds.
    ├── scripts:    Scripts for building network functions.
    └── ausf:       CMakefile.txt and object files for AUSF network function.
├── ci-scripts:     Scripts for CI framework.
├── docs:           Documentation files.
├── etc:            Configuration files for AUSF deployment.
└── src:            Source files for AUSF.
    ├── 5gaka:      Security algorithm implementations.
    ├── api-server: AUSF services APIs.
    ├── common:     Common header files.
    │   └── utils:  Common utilities.
    ├── oai_ausf:   Main directory, contains the "main" CMakeLists.txt.
    └── ausf_app:   Procedures and contexts for AUSF network functions.
```
