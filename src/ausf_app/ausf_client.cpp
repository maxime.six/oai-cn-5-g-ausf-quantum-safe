/*
 * Licensed to the OpenAirInterface (OAI) Software Alliance under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The OpenAirInterface Software Alliance licenses this file to You under
 * the OAI Public License, Version 1.1  (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.openairinterface.org/?page_id=698
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 * For more information about the OpenAirInterface (OAI) Software Alliance:
 *      contact@openairinterface.org
 */

/*! \file ausf_client.cpp
 \brief
 \author  Jian Yang, Fengjiao He, Hongxin Wang, Tien-Thinh NGUYEN
 \company Eurecom
 \date 2020
 \email:
 */

#include "ausf_client.hpp"

#include <curl/curl.h>
#include <openssl/ssl.h>
#include <nlohmann/json.hpp>
#include <pistache/http.h>
#include <pistache/mime.h>
#include <stdexcept>

#include "ausf.h"
#include "logger.hpp"

using namespace Pistache::Http;
using namespace Pistache::Http::Mime;
using namespace oai::ausf::app;
using namespace oai::config;
using json = nlohmann::json;

extern ausf_client* ausf_client_inst;
extern ausf_config ausf_cfg;

//static CURLcode ssl_ctx_callback(CURL *curl, void *ssl_ctx, void *userptr);

//------------------------------------------------------------------------------
// To read content of the response from NF
static std::size_t callback(
    const char* in, std::size_t size, std::size_t num, std::string* out) {
  const std::size_t totalBytes(size * num);
  out->append(in, totalBytes);
  return totalBytes;
}

//------------------------------------------------------------------------------
ausf_client::ausf_client() {}

//------------------------------------------------------------------------------
ausf_client::~ausf_client() {
  Logger::ausf_app().debug("Delete AUSF Client instance...");
}

//------------------------------------------------------------------------------
void ausf_client::curl_http_client(
    std::string remoteUri, std::string method, std::string msgBody,
    std::string& response) {
  Logger::ausf_app().info("Sending HTTP message to URI: %s with method: %s", remoteUri.c_str(), method.c_str());
  Logger::ausf_app().info("HTTP request body: %s", msgBody.c_str());

  uint32_t str_len = msgBody.length();
  char* body_data  = (char*) malloc(str_len + 1);
  memset(body_data, 0, str_len + 1);
  memcpy((void*) body_data, (void*) msgBody.c_str(), str_len);
  Logger::ausf_app().info("Body data prepared for transmission");

  curl_global_init(CURL_GLOBAL_ALL);
  CURL* curl = curl_easy_init();

  // // Set up the SSL context to use OQS
  // if (curl) {
  //   curl_easy_setopt(curl, CURLOPT_SSL_CTX_FUNCTION, *ssl_ctx_callback);
  // }

  Logger::ausf_app().info("CURL initialized");

  uint8_t http_version = 1;
  if (ausf_cfg.use_http2) {
    http_version = 2;
    Logger::ausf_app().info("Using HTTP/2 as per configuration");
  } else {
    Logger::ausf_app().info("Using HTTP/1.1 as per configuration");
  }



  if (curl) {
    CURLcode res               = {};
    struct curl_slist* headers = nullptr;
    if ((method.compare("POST") == 0) or (method.compare("PUT") == 0) or
        (method.compare("PATCH") == 0)) {
      std::string content_type = "Content-Type: application/json";
      headers = curl_slist_append(headers, content_type.c_str());
      curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
      Logger::ausf_app().info("HTTP headers set for POST/PUT/PATCH");
    }

    curl_easy_setopt(curl, CURLOPT_URL, remoteUri.c_str());
    Logger::ausf_app().info("CURL URL set: %s", remoteUri.c_str());

    if (method.compare("POST") == 0)
      curl_easy_setopt(curl, CURLOPT_HTTPPOST, 1);
    else if (method.compare("PUT") == 0)
      curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");
    else if (method.compare("DELETE") == 0)
      curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DELETE");
    else if (method.compare("PATCH") == 0)
      curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PATCH");
    else
      curl_easy_setopt(curl, CURLOPT_HTTPGET, 1);
    Logger::ausf_app().info("CURL method set: %s", method.c_str());

    curl_easy_setopt(curl, CURLOPT_TIMEOUT_MS, CURL_TIMEOUT_MS);
    curl_easy_setopt(curl, CURLOPT_TCP_KEEPALIVE, 1);
    curl_easy_setopt(curl, CURLOPT_INTERFACE, ausf_cfg.sbi.if_name.c_str());
    Logger::ausf_app().info("CURL network interface set: %s", ausf_cfg.sbi.if_name.c_str());

    const char* version = curl_version();
    Logger::ausf_app().info("libcurl version: %s", version);

    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
    // Configuration des certificats client pour mTLS
    curl_easy_setopt(curl, CURLOPT_SSLCERT, "/root/dilithium3_client.crt");
    curl_easy_setopt(curl, CURLOPT_SSLKEY, "/root/dilithium3_client.key");
    
    // Configuration de SSL/TLS
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L); // Activer la vérification du certificat du pair
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L); // Vérifier que le certificat correspond au host
    curl_easy_setopt(curl, CURLOPT_USE_SSL, CURLUSESSL_ALL); // Utiliser SSL/TLS
    

    std::unique_ptr<std::string> httpData(new std::string());
    std::unique_ptr<std::string> httpHeaderData(new std::string());

    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &callback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, httpData.get());
    curl_easy_setopt(curl, CURLOPT_HEADERDATA, httpHeaderData.get());
    Logger::ausf_app().info("CURL callback functions set");

    if ((method.compare("POST") == 0) or (method.compare("PUT") == 0) or
        (method.compare("PATCH") == 0)) {
      curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, msgBody.length());
      curl_easy_setopt(curl, CURLOPT_POSTFIELDS, body_data);
      Logger::ausf_app().info("CURL POST/PUT/PATCH body set");
    }
    res = curl_easy_perform(curl);

    long httpCode = {0};
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpCode);
    Logger::ausf_app().info("HTTP response code received: %ld", httpCode);

    response = *httpData.get();
    Logger::ausf_app().info("HTTP response body received: %s", response.c_str());

    if (httpCode == 0) {
      Logger::ausf_app().info("Failed to receive any HTTP response");
    }

    if (httpCode != HTTP_RESPONSE_CODE_OK &&
        httpCode != HTTP_RESPONSE_CODE_CREATED &&
        httpCode != HTTP_RESPONSE_CODE_NO_CONTENT) {
      Logger::ausf_app().warn("Received non-success HTTP code: %ld", httpCode);
    }

    curl_slist_free_all(headers);
    curl_easy_cleanup(curl);
    Logger::ausf_app().info("CURL cleanup completed");
  }

  

  curl_global_cleanup();
  Logger::ausf_app().info("Global CURL cleanup completed");

  if (body_data) {
    free(body_data);
    body_data = NULL;
    Logger::ausf_app().info("Body data memory freed");
  }
  Logger::ausf_app().info("Exiting curl_http_client function");
  return;
}

// // Example SSL context setup function using OQS
//   static CURLcode ssl_ctx_callback(CURL *curl, void *ssl_ctx, void *userptr) {
//     SSL_CTX *ctx = (SSL_CTX *)ssl_ctx;
//     // Configure the SSL context here with OQS-specific settings
//     SSL_CTX_set_cipher_list(ctx, "OQSKEM-DEFAULT:ECDHE-RSA-AES256-GCM-SHA384");
//     return CURLE_OK;
//   }